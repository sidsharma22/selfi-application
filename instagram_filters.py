
import cv2
import matplotlib.pyplot as plt
import numpy as np
%matplotlib inline
import matplotlib
matplotlib.rcParams['figure.figsize'] = (10.0, 10.0)
matplotlib.rcParams['image.cmap'] = 'gray'
def cartoonify(image, arguments=0):
#####################################################################
# 
# Description: This funtion would cartoonify the input image using edge detection techniques 
#              and return it.
# Input: image [array] : The image matrix on which cartoon filter has to be applied    
# Output: Cartoon Image created               
# 
######################################################################
    image_ep = cv2.edgePreservingFilter(image, flags=1, sigma_s=60, sigma_r=0.4)  # Performig edge preserving filter on the input image.
    im = pencilSketch(image)                                                      # Call pencil Jetck function to get the boundary sketch 
#     print(im.shape)                                                             # Printing the shape to check gray scale image ahs been returned
#     print(image.shape)                                                          # Printing the shape of the input image 
    pencilSketchImage = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)                      # Converting gray scale pencil sketch to BRG 
    cartoonImage = cv2.bitwise_and(pencilSketchImage,image_ep)                    # Anding pencil sketch with the gray scale image to get coartoon effect

    ###################
    return cartoonImage


def pencilSketch(image, arguments=0):
#########################################################################
#

# Description: This funciton would create a pencile sketch of the input image 
#              usign edge dectection techniques.
# Input: image [array]: The image matrix on which pencil filter has to be applied
# Output: Pencil sketch created
#
##########################################################################
    image_cr = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)                     # Converting the input BGR image into to GRAY image
    kernelSize = 1                                                         # Defining Kernel size for median blurr
    image_gb = cv2.medianBlur(image_cr,kernelSize)                         # Median Blur - To reduce the effect of small pixels with gradient values.
#     image_gb = cv2.GaussianBlur(image_cr, (5,5), 0, 0)                   # Gaussian Blur - To reduce the noise
    sobelx   = cv2.Sobel(image_gb, cv2.CV_32F, 1, 0,7)                     # Sobelx operation - Taking first order detivate with sobel matrix
                                                                           # to find the gradients along x directon
    sobely   = cv2.Sobel(image_gb, cv2.CV_32F, 0, 1,7)                     # Sobely opearation - to find gradient along y direction                                     
#     sobelx = cv2.Scharr( image_gb, cv2.CV_32F, 1, 0, 1, 0);              # Scharrx opearation - Tried Scharr filter which is know to give more accurate results.
#     sobely = cv2.Scharr( image_gb, cv2.CV_32F, 0, 1, 1, 0);              # Scharry opeartion
    cv2.normalize(sobelx,                                                  # Normalizing the sobel opertaion result
                  dst       = sobelx,                                      # Destination where the output should  be saved
                  alpha     = 0,                                           # Lower Limit of the normalization  
                  beta      = 1,                                           # Upper Limit of the normalization operation
                  norm_type = cv2.NORM_MINMAX,                             # Type of normalization performed
                  dtype     = cv2.CV_32F)                                  # Data type in which the normalizaed ale of the maritx will be stored 
    cv2.normalize(sobely,                                                  # Normalizing the sobel opertaion result
                 dst       = sobely,                                       # Destination where the output should  be saved
                 alpha     = 0,                                            # Lower Limit of the normalization 
                 beta      = 1,                                            # Upper Limit of the normalization operation
                 norm_type = cv2.NORM_MINMAX,                              # Type of normalization performed
                 dtype     = cv2.CV_32F)                                   # Data type in which the normalizaed ale of the maritx will be stored
#     cannyImage = cv2.Canny(image_gb, 50, 100)                            # Tried canny edge detection to view the results 
#     pencilSketchImage = cv2.bitwise_not(cannyImage)                      # Inverted the result of the canny edge detector to get a pencil sketch
#     sobelx = np.uint8(sobelx * 255)                                      # Converting normalized sobelx  [0,1] --> [0,255]
#     sobely = np.uint8(sobely * 255)                                      # Converting normalized sobely  [0,1] --> [0,255]
#     print(sobelx)                                                        # Printing Sobelx matirx to check the conversion
#     print(sobely)                                                        # Printing Sobely matirx to check the conversion
#     kernelSize = 3                                                       # Defining the kernel size for Laplacian operation for gradient detction 
#     laplacian = cv2.Laplacian(image_gb, cv2.CV_32F, ksize = kernelSize,  # Laplcian operation - This operation was giving results closer to the sample 
#                             scale = 1, delta = 0)                        # provided but not clear as   
#     cv2.normalize(laplacian,                                             # Normalizing the Laplacian result to [0,1] to incoporate the value greater than 255
#                  dst       = laplacian,                                  # Destination var where the result has to be stored
#                  alpha     = 0,                                          # Lower limit of normalization
#                  beta      = 1,                                          # Upper Limit of the normalzation
#                  norm_type = cv2.NORM_MINMAX,                            # Type of normalization carried out
#                  dtype     = cv2.CV_32F)                                 # Data type of the normalizaed matirx
#     pencilSketchImage = laplacian                                        # Assigning laplician operation valut to the 
#     plt.imshow(laplacian)
#     pencilSketchImage = np.uint8(pencilSketchImage * 255)
    pencilSketchImage = cv2.addWeighted(sobelx, 0.5, sobely, 0.5, 0)
#     plt.imshow(sobelx)
    pencilSketchImage = np.uint8(pencilSketchImage * 255)
    ret_val,th = cv2.threshold(pencilSketchImage, 121,255,cv2.THRESH_BINARY) 
#    cartoonImage = cv2.equalizeHist(pencilSketchImage)
#     kernel = np.ones((5,5),np.uint8)
#     cartoonImage = cv2.morphologyEx(pencilSketchImage, cv2.MORPH_OPEN, kernel)
    
#     pencilSketchImage = np.ones_like(image)                              # Create a matrix with all white pixels 
#     pencilSketchImage = cv2.add(pencilSketchImage,result)                # Copy the mask onto the matrix
    pencilSketchImage = th                                                 # Assigning the threshold value to the the return variable

    #plt.imshow(pencilSketchImage)

    return pencilSketchImage

imagePath = "trump.jpg"
image     = cv2.imread(imagePath)


pencilSketchImage = pencilSketch(image)
cartoonImage = cartoonify(image)

plt.figure(figsize=[20,10])
plt.subplot(131);plt.imshow(image[:,:,::-1]);
plt.subplot(132);plt.imshow(cartoonImage[:,:,::-1]);
plt.subplot(133);plt.imshow(pencilSketchImage);
